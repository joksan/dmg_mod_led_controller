#include "eeprom.h"
#include <avr/io.h>

uint8_t eeprom_read(uint16_t addr) {
  //Wait for completion of previous write.
  while(EECR & _BV(EEPE));

  //Set up address register.
  EEAR = addr;

  //Start eeprom read by writing EERE.
  EECR |= _BV(EERE);

  //Return data from Data Register.
  return EEDR;
}

void eeprom_write(uint16_t addr, uint8_t data) {
  //Wait for completion of previous write.
  while(EECR & _BV(EEPE));

  //Set up address and Data Registers.
  EEAR = addr;
  EEDR = data;

  //Write logical one to EEMPE.
  EECR |= _BV(EEMPE);

  //Start eeprom write by setting EEPE.
  EECR |= _BV(EEPE);
}
