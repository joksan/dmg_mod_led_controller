#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include "gpio.h"
#include "pwm.h"

void pwm_init_16(bool channel_a, bool channel_b) {
  //Configure channel pins as outputs, if needed.
  if (channel_a) {
    gpio_config_output(D9);
  }

  if (channel_b) {
    gpio_config_output(D10);
  }

  //Initialize Timer/Counter1 for PWM operation.
  TCCR1A = (channel_a? _BV(COM1A1): 0) | (channel_b? _BV(COM1B1):0) | _BV(WGM11);
  TCCR1B = _BV(WGM13) | _BV(CS10);
  ICR1 = 10000;
}

void pwm_write_16(uint8_t pin, uint16_t dc) {
  switch (pin) {
    case D9_OC1A:
      OCR1A = dc;
      break;
    case D10_OC1B:
      OCR1B = dc;
      break;
    default:
      return;
  }
}
