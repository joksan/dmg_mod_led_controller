#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 1000000
#include <util/delay.h>

#include "gpio.h"
#include "periph_mapping.h"
#include "button_reader.h"
#include "led_handler.h"

#include "avr/sleep.h"

static void config_unused_pins(void) {
  //Declare a struct that can be used with de UNUSED_PINS macro, then populate a variable with its
  //contents.
  const struct {
    uint8_t number;
    bool state;
  } unused_pins[] = { UNUSED_PINS };

  //Configure every unused pin as indicated.
  for (uint8_t i = 0; i < sizeof unused_pins / sizeof unused_pins[0]; i++) {
    gpio_config_output(unused_pins[i].number);
    gpio_write(unused_pins[i].number, unused_pins[i].state);
  }
}

int main(void) {
  //Configure all unused pins so they don't drain power when left open.
  config_unused_pins();

  //Reduce power consumption by powering off unused peripherals.
  PRR = _BV(PRADC) | _BV(PRUSART0) | _BV(PRSPI) | _BV(PRTIM0) | _BV(PRTWI);

  //Initialize functional submodules.
  button_reader_init();
  led_handler_init();

  //Enable interrupts globally so external button signals can be captured.
  sei();

  for (;;) {
    //Update button and LED logic.
    button_reader_update();
    led_handler_update();

    //Perform an small delay between loops.
    _delay_ms(20);
  }
}
