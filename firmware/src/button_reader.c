#include <avr/io.h>
#include <avr/interrupt.h>
#include "gpio.h"
#include "periph_mapping.h"
#include "button_reader.h"

//How fast auto-repeatable buttons are triggered.
#define REPEAT_PERIOD 10

buttons_t curr_b;   //Current button states (active for as long as pressed)
buttons_t pres_b;   //Button-just-pressed state (active for one period, also active if repeating)

//Pad ISR.
ISR(INT0_vect) {
  //When the pad select signal is asserted, capture the pad button states.
  curr_b.pad_u8 = READ_BUTTONS_PARALLEL();
}

//Button ISR.
ISR(INT1_vect) {
  //When the button select signal is asserted, capture the button states.
  curr_b.button_u8 = READ_BUTTONS_PARALLEL();
}

void button_reader_init(void) {
  //Initialize all pins for reading buttons.
  gpio_config_input(PIN_PAD);
  gpio_config_input(PIN_BUTTON);
  gpio_config_input(PIN_LEFT_B);
  gpio_config_input(PIN_DOWN_START);
  gpio_config_input(PIN_UP_SELECT);
  gpio_config_input(PIN_RIGHT_A);

  //Enable external interrupts for both pad and button select signals.
  EICRA = _BV(ISC11) | _BV(ISC01);   //Configure both interrupts to falling edge
  EIMSK = _BV(INT1) | _BV(INT0);
}

void button_reader_update(void) {
  static buttons_t prev_b;          //Previous button states
  static uint8_t rep_count = 0;   //Auto-repeat counter

  //Detect recent button presses.
  pres_b.pad_u8 = ~prev_b.pad_u8 & curr_b.pad_u8;
  pres_b.button_u8 = ~prev_b.button_u8 & curr_b.button_u8;

  //Increase the auto-epeat counter if pressing either up or down on the DPAD.
  //Reset the auto-repeat counter Otherwise.
  if (curr_b.up || curr_b.down) {
    rep_count = rep_count < REPEAT_PERIOD - 1? rep_count + 1: 0;
  }
  else {
    rep_count = 0;
  }

  //Provide repeated presses of up or down when the auto-repeat counter reaches its end.
  if (rep_count == REPEAT_PERIOD - 1) {
    pres_b.up = curr_b.up? 1: 0;
    pres_b.down = curr_b.down? 1: 0;
  }

  //Remember previous state for next time.
  prev_b.pad_u8 = curr_b.pad_u8;
  prev_b.button_u8 = curr_b.button_u8;
}
