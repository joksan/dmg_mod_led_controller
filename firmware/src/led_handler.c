#include <stdint.h>
#include <stdbool.h>
#include "eeprom.h"
#include "pwm.h"
#include "periph_mapping.h"
#include "button_reader.h"

//EEPROM addresses for non-volatile settings/states.
#define EE_ADDR_ANIMATION 0
#define EE_ADDR_ENABLED 1
#define EE_ADDR_BRIGHTNESS 2

//Configurable settings.
#define MAX_BRIGHTNESS 10   //How many brightness levels are available
#define PWM_SPEED 5         //How fast breathing animation goes

/*
PWM table for exponential brightness (looks natural to the eye).
Can be generated using this python code:

MAX_BRIGHTNESS = 10
base = pow(10000,1/MAX_BRIGHTNESS)
values = [0]
values.extend(round(pow(base, x)) for x in range(1, MAX_BRIGHTNESS + 1))
print("".join(["{},\n".format(x) for x in values]))
*/
static const uint16_t pwm_table[MAX_BRIGHTNESS + 1] = {
  0,
  3,
  6,
  16,
  40,
  100,
  251,
  631,
  1585,
  3981,
  10000,
};

//Available animation states.
typedef enum {
  a_on_all,
  a_on_red,
  a_on_blue,
  a_breathing,
  a_first = a_on_all,   //Last 2 enumerationss used for animation cycling
  a_last = a_breathing,
} animation_t;

//Local state variables.
static uint8_t animation;
static bool enabled;
static uint8_t brightness;
static uint16_t step;
static bool dir;

void led_handler_init(void) {
  //Initialize the 16-bit pwm timer, using both channels.
  pwm_init_16(true, true);

  //Read settings from EEPROM.
  animation = (animation_t) eeprom_read(EE_ADDR_ANIMATION);
  enabled = eeprom_read(EE_ADDR_ENABLED);
  brightness = eeprom_read(EE_ADDR_BRIGHTNESS);

  //Correct potentially invalid settings.
  animation = animation > a_last? a_first: animation;
  brightness = brightness > MAX_BRIGHTNESS? MAX_BRIGHTNESS: brightness;
}

void led_handler_update(void) {
  //Update button controls.
  if (curr_b.select) {
    //Update adjustments while enabled.
    if (enabled) {
      //Update animation changes.
      if (pres_b.right) {
        animation = animation < a_last? animation + 1: a_first;
        eeprom_write(EE_ADDR_ANIMATION, animation);
        if (animation == a_breathing) {
          step = 0;
          dir = false;
        }
      }

      //Disable the LEDs.
      if (pres_b.left) {
        enabled = false;
        eeprom_write(EE_ADDR_ENABLED, enabled);
      }

      //Raise brightness.
      if (pres_b.up && brightness < MAX_BRIGHTNESS) {
        brightness++;
        eeprom_write(EE_ADDR_BRIGHTNESS, brightness);
      }

      //Decrease brightness.
      if (pres_b.down && brightness > 0) {
        brightness--;
        eeprom_write(EE_ADDR_BRIGHTNESS, brightness);
      }
    }
    //Update adjustments while disabled.
    else {
      //Any pad press while disabled will cause the LEDs to be enabled again.
      if (pres_b.pad_u8 != 0) {
        enabled = true;
        eeprom_write(EE_ADDR_ENABLED, enabled);
      }
    }
  }

  //Update animation state.
  if (enabled) {
    switch (animation) {
      case a_on_all:
        pwm_write_16(PIN_LED_RED, pwm_table[brightness]);
        pwm_write_16(PIN_LED_BLUE, pwm_table[brightness]);
        break;
      case a_on_red:
        pwm_write_16(PIN_LED_RED, pwm_table[brightness]);
        pwm_write_16(PIN_LED_BLUE, 0);
        break;
      case a_on_blue:
        pwm_write_16(PIN_LED_RED, 0);
        pwm_write_16(PIN_LED_BLUE, pwm_table[brightness]);
        break;
      case a_breathing:
        //Check brightness change direction.
        if (!dir) {
          //Brightness is increasing (step goes up). Increase up to 256.
          step = step + PWM_SPEED > 256? 256: step + PWM_SPEED;

          //Switch directions once maximum is reached.
          if (step == 256) {
            dir = true;
          }
        }
        else {
          //Brightness is decreasing (step goes down). Decrease down to 0.
          step = step < PWM_SPEED? 0: step - PWM_SPEED;

          //Switch directions once minimum is reached.
          if (step == 0) {
            dir = false;
          }
        }

        pwm_write_16(PIN_LED_RED, pwm_table[brightness]);
        pwm_write_16(PIN_LED_BLUE, ((uint32_t) step * pwm_table[brightness]) >> 8);
        break;
    }
  }
  else {
    //LEDs are disabled.
    pwm_write_16(PIN_LED_RED, 0);
    pwm_write_16(PIN_LED_BLUE, 0);
  }
}
