#include <stddef.h>
#include <avr/io.h>
#include "gpio.h"

static volatile uint8_t *ddr_address(uint8_t pin) {
  switch (pin >> 4) {
#ifdef DDRB
    case 1:
      return &DDRB;
#endif  //DDRB
#ifdef DDRC
    case 2:
      return &DDRC;
#endif  //DDRC
#ifdef DDRD
    case 3:
      return &DDRD;
#endif  //DDRD
    default:
      return NULL;
  }
}

static volatile uint8_t *port_address(uint8_t pin) {
  switch (pin >> 4) {
#ifdef PORTB
    case 1:
      return &PORTB;
#endif  //PORTB
#ifdef PORTC
    case 2:
      return &PORTC;
#endif  //PORTC
#ifdef PORTD
    case 3:
      return &PORTD;
#endif  //PORTD
    default:
      return NULL;
  }
}

void gpio_config_input(uint8_t pin) {
  volatile uint8_t *ddr = ddr_address(pin);

  if (ddr == NULL) {
    return;
  }

  *ddr &= ~_BV(pin & 0x07);
}

void gpio_config_output(uint8_t pin) {
  volatile uint8_t *ddr = ddr_address(pin);

  if (ddr == NULL) {
    return;
  }

  *ddr |= _BV(pin & 0x07);
}

bool gpio_read(uint8_t pin) {
  volatile uint8_t *port = port_address(pin);

  if (port == NULL) {
    return false;
  }

  return *port & _BV(pin & 0x07)? true: false;
}

void gpio_write(uint8_t pin, bool value) {
  volatile uint8_t *port = port_address(pin);

  if (port == NULL) {
    return;
  }

  if (value) {
    *port |= _BV(pin & 0x07);
  }
  else {
    *port &= ~_BV(pin & 0x07);
  }
}
