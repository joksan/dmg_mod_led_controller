#ifndef BUTTON_READER_H_
#define BUTTON_READER_H_

#include <stdint.h>

//Structure used to store all kind of button states.
typedef struct {
  union {
    uint8_t pad_u8;
    struct {
      int left: 1;
      int down: 1;
      int up: 1;
      int right: 1;
    };
  };
  union {
    uint8_t button_u8;
    struct {
      int b: 1;
      int start: 1;
      int select: 1;
      int a: 1;
    };
  };
} buttons_t;

extern buttons_t curr_b;  //Current button states (active for as long as pressed)
extern buttons_t pres_b;  //Button-just-pressed state (active for one period, also active if repeating)

void button_reader_init(void);
void button_reader_update(void);

#endif //BUTTON_READER_H_
