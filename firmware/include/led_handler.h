#ifndef LED_HANDLER_H_
#define LED_HANDLER_H_

void led_handler_init(void);
void led_handler_update(void);

#endif //LED_HANDLER_H_
