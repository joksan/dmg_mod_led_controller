#ifndef GPIO_MAPPING_H_
#define GPIO_MAPPING_H_

#include "gpio.h"
#include "pwm.h"

//Pin settings for all LED signals.
#define PIN_LED_RED   D9_OC1A
#define PIN_LED_BLUE  D10_OC1B

//Pin settings for all button signals.
#define PIN_PAD        D2
#define PIN_BUTTON     D3
#define PIN_LEFT_B     D4
#define PIN_DOWN_START D5
#define PIN_UP_SELECT  D6
#define PIN_RIGHT_A    D7

//Macro used for reading button states in parallel (must match pin settings above).
#define READ_BUTTONS_PARALLEL() (__builtin_avr_swap(~PIND) & 0x0F)  //For using D4 through D7
//#define READ_BUTTONS_PARALLEL() (~(PINC) & 0x0F)                    //For using A0 through A3

#define UNUSED_PINS \
  { D0, true }, \
  { D1, true }, \
  { D8, false }, \
  { D12, false }, \
  { D13, false }, \
  { A0, false }, \
  { A1, false }, \
  { A2, false }, \
  { A3, false }, \
  { A4, false }, \
  { A5, false },

#endif //GPIO_MAPPING_H_
