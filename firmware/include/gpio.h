#ifndef GPIO_H_
#define GPIO_H_

#include <stdint.h>
#include <stdbool.h>

#define D8  0b00010000  //PB0
#define D9  0b00010001  //PB1
#define D10 0b00010010  //PB2
#define D11 0b00010011  //PB3
#define D12 0b00010100  //PB4
#define D13 0b00010101  //PB5

#define A0  0b00100000  //PC0
#define A1  0b00100001  //PC1
#define A2  0b00100010  //PC2
#define A3  0b00100011  //PC3
#define A4  0b00100100  //PC4
#define A5  0b00100101  //PC5

#define D0  0b00110000  //PD0
#define D1  0b00110001  //PD1
#define D2  0b00110010  //PD2
#define D3  0b00110011  //PD3
#define D4  0b00110100  //PD4
#define D5  0b00110101  //PD5
#define D6  0b00110110  //PD6
#define D7  0b00110111  //PD7

void gpio_config_input(uint8_t pin);
void gpio_config_output(uint8_t pin);
bool gpio_read(uint8_t pin);
void gpio_write(uint8_t pin, bool value);

#endif //GPIO_H_
