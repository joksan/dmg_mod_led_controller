#ifndef PWM_H_
#define PWM_H_

#include <stdint.h>
#include <stdbool.h>

//PWM output pins for 8-bit timers:
// #define D6_OC0A  0b00000000   //PD6
// #define D5_OC0B  0b00000001   //PD5
// #define D11_OC2A 0b00100000   //PB3
// #define D3_OC2B  0b00100001   //PD3

//PWM output pins for 16-bit timers:
#define D9_OC1A  0b00010000   //PB1
#define D10_OC1B 0b00010001   //PB2

void pwm_init_16(bool channel_a, bool channel_b);
void pwm_write_16(uint8_t pin, uint16_t dc);

#endif //PWM_H_
