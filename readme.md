A simplistic mod for the original gameboy (DMG) that adds fixed and animated LED lights.
========================================================================================

It's based on the arduino Nano board, which is small enough to fit in the rather large case of the
DMG.

Connection schematics can be found in the fritzing directory. Flex pins refer to the flex cable that
connects between the two boards. Ideally you would want to solder thin magnet wires to the flex
connector on the front board (the one that has the buttons and screen on it).

A firmware file for flashing can be found in the firmware/build directory. It can be flashed using
any AVR flashing tool (note: bootloaders aren't supported). An arduino running the arduino as ISP
sketch can be used along with avrdude for downloading the firmware to the board. Simply run the
following commands to set the fuses and flash the firmware, respectively:

```
$ make set_fuses
$ make flash
```

Whenever the gameboy CPU reads its buttons, the Arduino Nano will capture the button states as well,
allowing to control the LEDs without adding any buttons to the case.

Controls.
---------

| Input          | Action                |
|----------------|-----------------------|
| Select + up    | Increase brightness   |
| Select + down  | Decrease brightness   |
| Select + left  | Enable/disable lights |
| Select + right | Change animation/mode |
